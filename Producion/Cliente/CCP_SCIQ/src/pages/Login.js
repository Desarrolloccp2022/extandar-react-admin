import React from 'react'
import { Link } from 'react-router-dom';
import '../Components/styles/styles.css';



export default function Login() {
    return (
        <div className="pt-5 mt-5">  
  <h1 className="text-center"> Sistema de gestion de producción</h1>  
  <div className="container">  
    <div className="row">  
      <div className="col-md-5 mx-auto">  
        <div className="card card-body">  
          <form id="submitForm" action="#" method="post" data-parsley-validate data-parsley-errors-messages-disabled="true" noValidate _lpchecked={1}>  
            <input type="hidden" name="_csrf" defaultValue="7635eb83-1f95-4b32-8788-abec2724a9a4" />  
            <div className="form-group required">  
              <lsabel htmlFor="username">  Usuario </lsabel>  
              <input type="text" className="form-control text-lowercase" id="username" required name="username" defaultValue />  
            </div>                      
            <div className="form-group required">  
              <label className="d-flex flex-row align-items-center" htmlFor="password"> Contraseña 
                <a className="ml-auto border-link small-xl" href="#"> Recordar contraseña? </a> </label>  
              <input type="password" className="form-control" required id="password" name="password" defaultValue />  
            </div>  
            <div className="form-group mt-4 mb-4">  
              <div className="custom-control custom-checkbox">  
                <input type="checkbox" className="custom-control-input" id="remember-me" name="remember-me" data-parsley-multiple="remember-me" />  
                <label clss="custom-control-label" htmlFor="remember-me"> Recordar? </label>  
              </div>  
            </div>  
            <div className="form-group pt-1">  

                <Link to="/Home">

                 <button className="btn btn-warning btn-block text-white" >Ingresar</button>
                
                </Link>
             
              
              
                 
            </div>  
          </form>  
          
        </div>  
      </div>  
    </div>  
  </div>  
</div>


    )
}
