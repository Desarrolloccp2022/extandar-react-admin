
import Cabecera from '../Components/Cabecera';
import Aside from '../Components/Aside';
import Aside2 from '../Components/Aside2';
import Content from '../Components/Content';
import Footer from '../Components/Footer';
import Booklist from '../Components/Booklist';
import { Link } from 'react-router-dom';
import React, {Fragment, useState, useEffect} from 'react';
import "./styletable.css";


export default function Productos() {

    const [books, setBooks] = useState([])


      useEffect(() => {
    const getBooks = () => {
      fetch('http://localhost:9000/api')
      .then(res => res.json())
      .then(res => setBooks(res))
    }
    getBooks()
    
  }, [])


  return (

            <fragment>

            <Cabecera/>
            <Aside/>
            <Booklist

              books={books} 
            />
            <Aside2/>
            <Footer/>
            </fragment>

            
  )
}