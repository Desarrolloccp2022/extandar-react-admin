import React from 'react'
import Cabecera from '../Components/Cabecera';
import Aside from '../Components/Aside';
import Aside2 from '../Components/Aside2';
import Content from '../Components/Content';
import Footer from '../Components/Footer';



export default function Inicio() {
    return (
        <div className='wrapper'>
                <Cabecera/>
                <Aside/>
                <Content
                
                name ="Christian"
                />
                <Aside2/>
                <Footer/>

        </div>
    )
}
