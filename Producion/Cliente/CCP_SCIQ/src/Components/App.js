
import {Route,Routes} from "react-router-dom";
import Login from '../pages/Login';
import Inicio from '../pages/Inicio';
import Contacto from '../pages/Contacto';
import Productos from '../pages/Productos';




export default function App() {
    return (
        
        <div>

            <Routes>
            <Route  path="/" exact  element={<Login/>} />
            <Route  path="/Home"   element={<Inicio/>} />
            <Route  path="/contacto"   element={<Contacto/>} />
            <Route  path="/libros"   element={<Productos/>} />
           

            </Routes>


        </div>
            
        
        
        
    )
}
