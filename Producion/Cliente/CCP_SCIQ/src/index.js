import React from 'react';
import { render } from "react-dom";
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
// import 'bootstrap/dist/css/bootstrap.css';
import App from './Components/App';





const rootElement = document.getElementById("root");
render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  rootElement
);